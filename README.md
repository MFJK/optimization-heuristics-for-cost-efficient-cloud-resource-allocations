# Optimization Heuristics for Cost-Efficient Cloud Resource Allocations

This repository holds [data](./data) that was used for the evaluation of two algorithms: *Efficient Resource Inference for Cloud Hosting* (ERICH) and
*Genetic Optimizations of Resource Groupings* (GEORG).